# About Default Value Module

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------

This module provides default value for existing entities on load.

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the default_vaue module to the modules directory.

 2. Enable the 'default_vaue' module and desired sub-modules in 'Extend'.
   (/admin/modules)

CONFIGURATION
-------------

 * Configure your default value supported entities in below configuration page
 * Go to default value settings page.
    > Note: Admin -> Configuration -> System -> Default Value Settings
